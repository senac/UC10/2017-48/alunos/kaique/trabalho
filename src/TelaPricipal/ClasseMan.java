package TelaPricipal;
import Cliente.Cliente;
import Livro.Livro;
import java.util.ArrayList;
import java.util.*;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import Livro.*;
import genero.Genero;



public class ClasseMan {
    
    public static List <Cliente> listCliente = new ArrayList<>();
    public static List <genero> listGenero = new ArrayList<>();
    public static List <Autor> listAutor = new ArrayList<>();
    public static List <Livro> listLivro = new ArrayList<>();

    public static List<Cliente> getListCliente() {
        return listCliente;
    }

    public static void setListCliente(List<Cliente> listCliente) {
        ClasseMan.listCliente = listCliente;
    }

    public static List<genero> getListGenero() {
        return listGenero;
    }

    public static void setListGenero(List<genero> listGenero) {
        ClasseMan.listGenero = listGenero;
    }

    public static List<Autor> getListAutor() {
        return listAutor;
    }

    public static void setListAutor(List<Autor> listAutor) {
        ClasseMan.listAutor = listAutor;
    }

    public static List<Livro> getListLivro() {
        return listLivro;
    }

    public static void setListLivro(List<Livro> listLivro) {
        ClasseMan.listLivro = listLivro;
    }

    
    public static void main(String[] args) {
        
        new JFrameTelaPrincipal();
        
    }    
    
    public static void addCli (Cliente cliente){
        listCliente.add(cliente);
    }
    
    public static void addgenero (genero Genero){
        listGenero.add(Genero);
    }
    
    public static void addAutor (Autor autor){
        listAutor.add(autor);
    }
    
    public static void addLivro (Livro livro){
        listLivro.add(livro);
    }
    
     public static void adicionarLivro(Livro livro){
        listLivro.add(livro);
    }
    public static void removerLivro(Livro livro){
        listLivro.remove(livro);
    }
    public static void removerIndLivro(int indice){
        listLivro.size();
    }
    public static void adicionargenero(genero Genero){
        listGenero.add(Genero);
    }
    public static void removergenero(Genero genero){
        listGenero.remove(genero);
    }
    public static void removerIndgenero(int indice){
        listGenero.size();
    }
    public static void adicionarCliente(Cliente cliente){
        listCliente.add(cliente);
    }
    public static void removerCliente(Cliente cliente){
        listCliente.remove(cliente);
    }
    public static void removerIndCliente(int indice){
        listCliente.size();
    }
    public void adicionarAutor(Autor autor){
        listAutor.add(autor);
    }
    public void removerAutor(Autor autor){ 
        listAutor.remove(autor);
    }
    public void removerIndAutor(int indice){
        listAutor.size();
    }
}
    
    
    