package Livro;

import java.util.Date;

public class Emprestimo {

    private Date dataEmprestmo;
    private Date dataDevolucao;

    public Emprestimo(Date dataEmprestmo, Date dataDevolucao) {
        this.dataEmprestmo = dataEmprestmo;
        this.dataDevolucao = dataDevolucao;
    }

    public Date getDataEmprestmo() {
        return dataEmprestmo;
    }

    public void setDataEmprestmo(Date dataEmprestmo) {
        this.dataEmprestmo = dataEmprestmo;
    }

    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }
    
    
    
}
