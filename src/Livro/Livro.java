package Livro;
public class Livro {
    private String titulo;
    private int isbn;
    private int anoEdcao;
    private String editora;

    public Livro(String titulo, int isbn, int anoEdcao, String editora) {
        this.titulo = titulo;
        this.isbn = isbn;
        this.anoEdcao = anoEdcao;
        this.editora = editora;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public int getAnoEdcao() {
        return anoEdcao;
    }

    public void setAnoEdcao(int anoEdcao) {
        this.anoEdcao = anoEdcao;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }
    
    
    
    
}
