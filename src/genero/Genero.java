package genero;

public class Genero {

    private String acao;
    private String terro;
    private String ficcao;
    private String romance;

    public Genero(String acao, String terro, String ficcao, String romance) {
        this.acao = acao;
        this.terro = terro;
        this.ficcao = ficcao;
        this.romance = romance;
    }

    public String getAcao() {
        return acao;
    }

    public String getTerro() {
        return terro;
    }

    public String getFiccao() {
        return ficcao;
    }

    public String getRomance() {
        return romance;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public void setTerro(String terro) {
        this.terro = terro;
    }

    public void setFiccao(String ficcao) {
        this.ficcao = ficcao;
    }

    public void setRomance(String romance) {
        this.romance = romance;
    }

    
    
    
}
